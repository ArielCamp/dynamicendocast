# DynamicEndocast

DynamicEndocast is a set of codes for calculating and visualizing volumes enclosed by moving objects. It is designed to be used with X-ray Reconstruction of Moving Morphology (XROMM) skeletal animations, and measure biological volumes such as the mouth cavity or lung cavity. Components of the codes are written by Ariel Camp, Elska Kaczmarek, Peter Falkingham, and Stephen Gatesy.

DynamicEndocast uses both Matlab and Maya. In Matlab, the script calculates 3D polygons (using alpha shapes) from landmarks on moving bone models, and calculate the volume of each polygon. Then in Maya the script imports the polygons so the endocast can be visualized with the original XROMM animation.

Development of DynamicEndocast is supported by the US National Science Foundation through a Division of Integrative Organismal Systems grant to Elizabeth Brainerd and Ariel Camp.